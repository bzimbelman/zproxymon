// const http = require("http");
// const httpProxy = require("http-proxy");
// const zlib = require("zlib");

// const createProxyServer = () => {
//   const proxy = httpProxy.createProxyServer({ rejectUnauthorized: false });
//   var uniqueId = 0;

//   const processResult = (code, msg, control, body) => {
//     console.log(
//       `Res for ${control?.method}:${control?.url}(${control?.id}) from proxied server [${code}-${msg}]: \n`,
//       body
//     );
//   };

//   proxy.on("proxyRes", function (proxyRes, req, res) {
//     var bodyBuffers = [];
//     proxyRes.on("data", function (chunk) {
//       // console.log(`ondata chunk ${typeof chunk}`, chunk);
//       bodyBuffers.push(chunk);
//     });
//     proxyRes.on("end", function () {
//       const headers = proxyRes?.headers;
//       const contentEncoding = headers["content-encoding"];
//       // console.log(`Res from proxy server is encoded ${contentEncoding}`);
//       // console.log(`body buffer contains: `, bodyBuffers);

//       if (contentEncoding?.includes("gzip")) {
//         zlib.gunzip(Buffer.concat(bodyBuffers), (err, dezipped) => {
//           processResult(proxyRes?.statusCode, proxyRes.statusMessage, req?.control, dezipped.toString());
//         });
//       } else {
//         processResult(
//           proxyRes?.statusCode,
//           proxyRes.statusMessage,
//           req?.control,
//           Buffer.concat(bodyBuffers).toString()
//         );
//       }
//     });
//     proxyRes.on("error", function (err) {
//       console.log(`Proxyed Server responded with an error: `, err, body);
//     });
//   });
//   proxy.on("proxyReq", function (proxyReq, req, res) {
//     proxyReq.setHeader("origin", "https://portal.dev.icanbwell.com");
//     proxyReq.setHeader("referer", "https://portal.dev.icanbwell.com");
//     proxyReq.setHeader(
//       "user-agent",
//       "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_15_7) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/97.0.4692.71 Safari/537.36"
//     );
//   });
//   proxy.on("error", function (err, req, res) {
//     console.log(`proxy error caught: `, err);
//     res.writeHead(500, {
//       "Content-Type": "text/plain",
//     });
//     res.end(`Something went wrong on ${req?.method} ${req?.url} response: ${res?.statusCode}`, err);
//   });

//   const server = http.createServer(async function (req, res) {
//     console.log(`Request ${req?.method} ${req?.url} ${uniqueId} received: `, req.headers);
//     req.control = { method: req?.method, url: req?.url, id: uniqueId++ };

//     proxy.web(req, res, {
//       target: "https://platform-api.dev-ue1.icanbwell.com",
//       selfHandleResponse: false,
//       changeOrigin: true,
//     });
//   });
// };

// const startProxyServer = (port) => {
//   console.log(`Server starting on ${port}`);
//   server.listen(port);
// };

// export { createProxyServer, startProxyServer };
