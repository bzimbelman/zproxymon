// import React, { useState } from "react";
import { Box, Text, Row, Switch } from "native-base";
import { useAtom } from "jotai";
import { proxyAtom } from "../App";

// const proxyAtom = atom(false);

function ToggleProxy() {
  // const [proxy, setProxy] = useState(false);
  const [proxy, setProxy] = useAtom(proxyAtom);
  // const [proxyCallback, setProxyCallback] = useAtom(proxyCallbackAtom);
  console.log(`ToggleProxy: `, proxy);
  const toggleProxy = () => {
    setProxy(proxy ? false : true);
    // proxyCallback();
  };
  return (
    <Box paddingLeft="3">
      <Row>
        <Text>Proxy</Text>
        <Switch isChecked={proxy} onToggle={toggleProxy} aria-label={proxy ? "run proxy" : "turn off proxy"} />
      </Row>
    </Box>
  );
}

export default ToggleProxy;
