import { Box, Text, Row, Switch, useColorMode } from "native-base";

function ToggleDarkMode() {
  const { colorMode, toggleColorMode } = useColorMode();
  return (
    <Box bg={colorMode === "light" ? "coolGray.400" : "coolGray.500"} borderRadius={9}>
      <Row>
        <Text paddingX={2}>Dark</Text>
        <Switch
          isChecked={colorMode === "light" ? true : false}
          onToggle={toggleColorMode}
          aria-label={colorMode === "light" ? "switch to dark mode" : "switch to light mode"}
        />
        <Text paddingX={2}>Light</Text>
      </Row>
    </Box>
  );
}

export default ToggleDarkMode;
