import React from "react";
import { Box, Text, Row, Column } from "native-base";

export interface LogEntryInterface {
  contents: string;
}

export const LogEntry: React.FC<LogEntryInterface> = ({ contents }) => {
  console.log(`LOG CONTENTS: `, contents);
  const content = JSON.parse(contents)?.content;
  return (
    <Box>
      <Row>
        <Column flex={1} alignItems="flex-start" margin="1">
          <Text>
            {content.from}-{content.name}
          </Text>
        </Column>
        <Column flex={4} alignItems="flex-start" margin="1">
          <Text>{content.msg}</Text>
        </Column>
      </Row>
    </Box>
  );
};
