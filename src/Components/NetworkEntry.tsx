import React from "react";
import { Box, Text, Row, Column } from "native-base";

export interface NetworkEntryInterface {
  name: string;
  status: string;
  id: string;
  type: string;
  size: string;
  time: string;
  description: string;
}

export const NetworkEntry: React.FC<NetworkEntryInterface> = ({ name, status, id, type, size, time, description }) => {
  return (
    <Box>
      <Row>
        <Column flex={6} alignItems="flex-start" margin="1">
          <Text>{name}</Text>
        </Column>
        <Column flex={4} alignItems="flex-start" margin="1">
          <Text>{status}</Text>
        </Column>
        <Column flex={2} alignItems="flex-start" margin="1">
          <Text>{type}</Text>
        </Column>
        <Column flex={1} alignItems="flex-start" margin="1">
          <Text>{id}</Text>
        </Column>
        <Column flex={3} alignItems="flex-start" margin="1">
          <Text>{size}</Text>
        </Column>
        <Column flex={4} alignItems="flex-start" margin="1">
          <Text>{time}</Text>
        </Column>
        <Column flex={20} alignItems="flex-start" margin="1">
          <Text>{description}</Text>
        </Column>
      </Row>
    </Box>
  );
};
