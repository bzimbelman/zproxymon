import { Box, Text, Row, Column, useColorMode } from "native-base";

function NetworkHeader() {
  const { colorMode } = useColorMode();
  const bgc = colorMode === "light" ? "coolGray.200" : "coolGray.700";
  return (
    <Box>
      <Row>
        <Column flex={6} alignItems="center" bg={bgc} margin="1">
          <Text>Name</Text>
        </Column>
        <Column flex={4} bg={bgc} alignItems="center" margin="1">
          <Text>Status</Text>
        </Column>
        <Column flex={2} bg={bgc} alignItems="center" margin="1">
          <Text>Type</Text>
        </Column>
        <Column flex={1} bg={bgc} alignItems="center" margin="1">
          <Text>ID</Text>
        </Column>
        <Column flex={3} bg={bgc} alignItems="center" margin="1">
          <Text>Size</Text>
        </Column>
        <Column flex={4} bg={bgc} alignItems="center" margin="1">
          <Text>Time</Text>
        </Column>
        <Column flex={20} bg={bgc} alignItems="center" margin="1">
          <Text>Description</Text>
        </Column>
      </Row>
    </Box>
  );
}

export default NetworkHeader;
