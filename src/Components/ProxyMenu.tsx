import { Box, Text, Row, useColorMode, Column } from "native-base";

function ProxyMenu() {
  const { colorMode } = useColorMode();

  return (
    <Box>
      <Row>
        <Column alignItems="center" marginX={2}>
          <Text paddingX={2}>List of Proxies</Text>
        </Column>
        <Column alignItems="center" marginX={2}>
          <Text paddingX={2}>End Of Menu</Text>
        </Column>
      </Row>
    </Box>
  );
}

export default ProxyMenu;
