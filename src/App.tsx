import React, { useState, useEffect, useRef } from "react";
import { Box, Row, useColorMode, Column, Text } from "native-base";
import ToggleDarkMode from "./Components/ToggleDarkMode";
import ProxyMenu from "./Components/ProxyMenu";
import NetworkHeader from "./Components/NetworkHeader";
import { NetworkEntry } from "./Components/NetworkEntry";
import { LogEntry } from "./Components/LogEntry";
import { Provider, atom, useAtom } from "jotai";
import { w3cwebsocket as W3CWebSocket } from "websocket";

export const proxyAtom = atom(false);

const client = new W3CWebSocket("ws://127.0.0.1:8889");
console.log(`WS Client: `, client);

const ScrollToBottom = () => {
  const elementRef = useRef<HTMLDivElement>(null);
  useEffect(() => {
    if (elementRef.current != null) {
      elementRef.current.scrollIntoView();
    }
  });
  return <div ref={elementRef} />;
};

function AppContents() {
  const { colorMode } = useColorMode();
  const buildNetworkEntry = (content: any) => {
    return {
      name: content.url,
      key: content.name,
      status: `${content.code || " "} ${content.message || " "}`,
      id: content.id,
      type: content.method,
      size: "0",
      time: "now",
      description: content.body,
    };
  };

  const buildLogEntry = (contents: any) => {
    return {
      type: "log",
      key: JSON.stringify(contents),
      contents: contents,
    };
  };

  const [messages, setMessages] = useState<any>([buildNetworkEntry({ name: "First", desc: "First Message" })]);
  const [count, setCount] = useState(1);

  useEffect(() => {
    console.log(`UE...`);
  });

  client.onopen = () => {
    console.log(`ws client connected`);
    client.send("Initialized...");
  };
  client.onmessage = (message) => {
    console.log(`ws msg: `, message);
    const msgObj = JSON.parse(message.data.toString());
    console.log(`ONMESSAGE ${msgObj.type}: `, msgObj);
    const msgs = messages;
    var newMessage = null;
    if (msgObj.type === "LOG") {
      newMessage = buildLogEntry(message.data.toString());
    } else {
      const content = msgObj?.content;
      newMessage = buildNetworkEntry(content);
    }
    msgs.push(newMessage);
    setMessages(msgs);
    setCount(count + 1);
  };

  console.log(`messages: `, messages);
  return (
    <Box>
      <Box bg={colorMode === "light" ? "coolGray.200" : "coolGray.700"} paddingTop={2} paddingBottom={3} px={4}>
        <Column>
          <Row>
            <Column flex="10" alignItems="flex-start">
              <ProxyMenu />
            </Column>
            <Column flex="2" alignItems="flex-end">
              <ToggleDarkMode />
            </Column>
          </Row>
        </Column>
      </Box>
      <Box bg={colorMode === "light" ? "coolGray.50" : "coolGray.900"} minHeight="100vh" px={4}>
        <Text>
          {messages.length} - {count}
        </Text>
        <NetworkHeader />

        {messages.map((m: any) =>
          m.type === "log" ? (
            <LogEntry contents={m.contents} key={m.key} />
          ) : (
            <NetworkEntry
              name={m.name}
              key={m.key}
              status={m.status}
              id={m.id}
              type={m.type}
              size={m.size}
              time={m.time}
              description={m.description}
            />
          )
        )}
        <ScrollToBottom />
      </Box>
    </Box>
  );
}

function App() {
  return (
    <Provider>
      <AppContents />
    </Provider>
  );
}

export default App;
